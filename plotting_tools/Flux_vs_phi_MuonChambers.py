import numpy as np
import matplotlib as plt
import math
import pylab
import sys 

#geometry_location="C:/Users/Alfredo/source/repos/RadSim/geometries/v_3_0_0_0"
#geometry_location="C:/Users/Alfredo/source/repos/RadSim/geometries/v_3_31_4_0"
geometry_location="C:/Users/Alfredo/source/repos/RadSim/geometries/v_3_31_4_0/fulletarange"

#sensitivityneut= float(sys.argv[1])
#sensitivityph= float(sys.argv[2])
#sensitivityepem= float(sys.argv[3])

sensitivityneut = 0.0026
sensitivityph = 0.016
sensitivityepem = 0.35

#sensitivityneutDT = 0.001
#sensitivityphDT = 0.01
#sensitivityepemDT = 0.30

sensitivityneut = 1.0
sensitivityph = 1.0
sensitivityepem = 1.0

Lumi = 800000000.0*1.5

#####  limits on Z and R ranges 
phiin_MB1 = np.loadtxt(geometry_location+"/MB1_neut_phi.dat",usecols=(0))
phifin_MB1 = np.loadtxt(geometry_location+"/MB1_neut_phi.dat",usecols=(1))

phi_center = (phiin_MB1+phifin_MB1)/2.

phiin_ME1 = np.loadtxt(geometry_location+"/ME1213_neut_phi.dat",usecols=(0))
phifin_ME1 = np.loadtxt(geometry_location+"/ME1213_neut_phi.dat",usecols=(1))

phi_center_ME1 = (phiin_ME1+phifin_ME1)/2.


####### Flux DT chambers 
flux_MB1_neut = np.loadtxt(geometry_location+"/MB1_neut_phi.dat",usecols=(2))*Lumi
flux_MB2_neut = np.loadtxt(geometry_location+"/MB2_neut_phi.dat",usecols=(2))*Lumi
flux_MB3_neut = np.loadtxt(geometry_location+"/MB3_neut_phi.dat",usecols=(2))*Lumi
flux_MB4_neut = np.loadtxt(geometry_location+"/MB4_neut_phi.dat",usecols=(2))*Lumi

###### Flux RPC chambers

flux_RE1_neut = np.loadtxt(geometry_location+"/RE1213_neut_phi.dat",usecols=(2))*Lumi
flux_RE2_neut = np.loadtxt(geometry_location+"/RE2122_neut_phi.dat",usecols=(2))*Lumi
flux_RE3_neut = np.loadtxt(geometry_location+"/RE3233_neut_phi.dat",usecols=(2))*Lumi
flux_RE4_neut = np.loadtxt(geometry_location+"/RE4243_neut_phi.dat",usecols=(2))*Lumi

###### Flux RPC chambers (barrel)

flux_RB1_neut = np.loadtxt(geometry_location+"/RB1top_neut_phi.dat",usecols=(2))*Lumi
flux_RB2_neut = np.loadtxt(geometry_location+"/RB2top_neut_phi.dat",usecols=(2))*Lumi
flux_RB3_neut = np.loadtxt(geometry_location+"/RB3_neut_phi.dat",usecols=(2))*Lumi
flux_RB4_neut = np.loadtxt(geometry_location+"/RB4_neut_phi.dat",usecols=(2))*Lumi

##### Flux CSC chambers 

flux_ME1_neut = np.loadtxt(geometry_location+"/ME1213_neut_phi.dat",usecols=(2))*Lumi
flux_ME2_neut = np.loadtxt(geometry_location+"/ME2_neut_phi.dat",usecols=(2))*Lumi
flux_ME3_neut = np.loadtxt(geometry_location+"/ME3_neut_phi.dat",usecols=(2))*Lumi
flux_ME4_neut = np.loadtxt(geometry_location+"/ME4_neut_phi.dat",usecols=(2))*Lumi

#CMSGeomTag = "CMS-FLUKA study v.3.0.0.0"  
CMSGeomTag = "CMS-FLUKA study v.3.31.4.0" 
    
plt.pyplot.figure(1)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (30,10**4) )
else:
    plt.pyplot.ylim( (10**0,10**3) )

plt.pyplot.plot(phi_center,flux_MB1_neut,label='MB1',color="red",marker="^",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(phi_center,flux_MB1_neut,label='MB2',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(phi_center,flux_MB1_neut,label='MB3',color="red",marker="s",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(phi_center,flux_MB1_neut,label='MB4',color="red",marker="v",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16,ncol=2)

plt.pyplot.xlabel(r'$\phi$',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Neutron Flux[Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/DTs_vs_phi_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/DTs_vs_phi_rate.png")



plt.pyplot.figure(2)
plt.pyplot.ylim( (0.1,40) )

plt.pyplot.plot(phi_center_ME1,flux_RE1_neut/flux_ME1_neut,label='RE1/ME1',color="red",marker="^",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
#plt.pyplot.plot(phi_center_ME1,flux_RE2_neut/flux_ME2_neut,label='RE1/ME1',color="red",marker="o",linestyle="none")
#plt.pyplot.yscale('log')
#plt.pyplot.plot(phi_center_ME1,flux_RE3_neut/flux_ME3_neut,label='RE1/ME1',color="red",marker="s",fillstyle="none",linestyle="none")
#plt.pyplot.yscale('log')
#plt.pyplot.plot(phi_center_ME1,flux_RE4_neut/flux_ME4_neut,label='RE1/ME1',color="red",marker="v",linestyle="none")
#plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16,ncol=2)


plt.pyplot.xlabel(r'$\phi$',fontsize=18,horizontalalignment='right',x=1.0)
plt.pyplot.ylabel("Ratio RPC/DT",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.savefig(geometry_location+"/plots/ratio_RE1_ME1_phi.png")


plt.pyplot.figure(3)
plt.pyplot.ylim( (0.4,40) )

plt.pyplot.plot(phi_center,flux_RB1_neut/flux_MB1_neut,label='RB1/MB1',color="red",marker="^",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(phi_center,flux_RB2_neut/flux_MB2_neut,label='RB2/MB2',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(phi_center,flux_RB3_neut/flux_MB3_neut,label='RB3/MB3',color="red",marker="s",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(phi_center,flux_RB4_neut/flux_MB4_neut,label='RB4/MB4',color="red",marker="v",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16,ncol=2)

plt.pyplot.xlabel(r'$\phi$',fontsize=18,horizontalalignment='right',x=1.0)
plt.pyplot.ylabel("Ratio RPC/DT",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.savefig(geometry_location+"/plots/ratio_RE_MB_phi.png")


plt.pyplot.show()

