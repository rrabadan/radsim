import pandas as pd 
import matplotlib.pyplot as plt
import math
import pylab


geometry_location="C:/Users/Alfredo/source/repos/RadSim/geometries/v_3_31_4_0/"
geomTag="v3_31_4_0_"


#stations = ["22","24","26","28","30","32"]


stations = ["22"]

pd.set_option('display.max_rows', 300)
colnames=["e_bin_left","e_bin_right","dfluxdE","error"]

Volume = []


for station in stations:
    if station == "22":
       Volume= 3484136.209 


    if station != "28":
        data_neut = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=list(range(0,2))+list(range(264,266)),nrows=360,names=colnames)
        data_ph = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,370),nrows=100,names=colnames)
        data_epem = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,474),nrows=100,names=colnames)
        #print(data_neutrons)
        #print(data_ph)
        #print(data_epem)

        data_neut["bin_center"] = (data_neut["e_bin_right"] + data_neut["e_bin_left"])/2. 
        data_neut["E(dflux/dE)"] = (data_neut["bin_center"]*data_neut["dfluxdE"])
        data_neut["dflux/dE"] = data_neut["dfluxdE"]

        data_ph["bin_center"] = (data_ph["e_bin_right"] + data_ph["e_bin_left"])/2. 
        data_ph["E(dflux/dE)"] = data_ph["bin_center"]*data_ph["dfluxdE"]
        data_ph["dflux/dE"] = data_ph["dfluxdE"]

        data_epem["bin_center"] = (data_epem["e_bin_right"] + data_epem["e_bin_left"])/2. 
        data_epem["E(dflux/dE)"] = data_epem["bin_center"]*data_epem["dfluxdE"]
        data_epem["dflux/dE"] = data_epem["dfluxdE"]

        data_neut["dflux"] = (data_neut["e_bin_right"] - data_neut["e_bin_left"])*data_neut["dfluxdE"]/Volume
        # Total Flux [Hz/cm^{2}] L=1x10E34cm-2s-1
        FluxNeut= data_neut["dflux"].sum()*800000000
        print(FluxNeut) 



    else: 
        data_neut_RE21 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=list(range(0,2))+list(range(264,266)),nrows=360,names=colnames)
        data_ph_RE21 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,370),nrows=100,names=colnames)
        data_epem_RE21 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,474),nrows=100,names=colnames)

        data_neut_RE22 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=list(range(0,578))+list(range(839,842)),nrows=360,names=colnames)
        data_ph_RE22 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,946),nrows=100,names=colnames)
        data_epem_RE22 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,1050),nrows=100,names=colnames)

        data_neut_RE31 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=list(range(0,1154))+list(range(1415,1418)),nrows=360,names=colnames)
        data_ph_RE31 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,1522),nrows=100,names=colnames)
        data_epem_RE31 = pd.read_table(geometry_location+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,1626),nrows=100,names=colnames)

        #print(data_ph_RE31)

        data_neut_RE21["bin_center"] = (data_neut_RE21["e_bin_right"] + data_neut_RE21["e_bin_left"])/2. 
        data_neut_RE21["E(dflux/dE)"] = data_neut_RE21["bin_center"]*data_neut_RE21["dfluxdE"]
        data_neut_RE21["dflux/dE"] = data_neut_RE21["dfluxdE"]

        data_ph_RE21["bin_center"] = (data_ph_RE21["e_bin_right"] + data_ph_RE21["e_bin_left"])/2. 
        data_ph_RE21["E(dflux/dE)"] = data_ph_RE21["bin_center"]*data_ph_RE21["dfluxdE"]
        data_ph_RE21["dflux/dE"] = data_ph_RE21["dfluxdE"]

        data_epem_RE21["bin_center"] = (data_epem_RE21["e_bin_right"] + data_epem_RE21["e_bin_left"])/2. 
        data_epem_RE21["E(dflux/dE)"] = data_epem_RE21["bin_center"]*data_epem_RE21["dfluxdE"]
        data_epem_RE21["dflux/dE"] = data_epem_RE21["dfluxdE"]

        data_neut_RE22["bin_center"] = (data_neut_RE22["e_bin_right"] + data_neut_RE22["e_bin_left"])/2. 
        data_neut_RE22["E(dflux/dE)"] = data_neut_RE22["bin_center"]*data_neut_RE22["dfluxdE"]
        data_neut_RE22["dflux/dE"] = data_neut_RE22["dfluxdE"]

        data_ph_RE22["bin_center"] = (data_ph_RE22["e_bin_right"] + data_ph_RE22["e_bin_left"])/2. 
        data_ph_RE22["E(dflux/dE)"] = data_ph_RE22["bin_center"]*data_ph_RE22["dfluxdE"]
        data_ph_RE22["dflux/dE"] = data_ph_RE22["dfluxdE"]

        data_epem_RE22["bin_center"] = (data_epem_RE22["e_bin_right"] + data_epem_RE22["e_bin_left"])/2. 
        data_epem_RE22["E(dflux/dE)"] = data_epem_RE22["bin_center"]*data_epem_RE22["dfluxdE"]
        data_epem_RE22["dflux/dE"] = data_epem_RE22["dfluxdE"]

        data_neut_RE31["bin_center"] = (data_neut_RE31["e_bin_right"] + data_neut_RE31["e_bin_left"])/2. 
        data_neut_RE31["E(dflux/dE)"] = data_neut_RE31["bin_center"]*data_neut_RE31["dfluxdE"]
        data_neut_RE31["dflux/dE"] = data_neut_RE31["dfluxdE"]

        data_ph_RE31["bin_center"] = (data_ph_RE31["e_bin_right"] + data_ph_RE31["e_bin_left"])/2. 
        data_ph_RE31["E(dflux/dE)"] = data_ph_RE31["bin_center"]*data_ph_RE31["dfluxdE"]
        data_ph_RE31["dflux/dE"] = data_ph_RE31["dfluxdE"]

        data_epem_RE31["bin_center"] = (data_epem_RE31["e_bin_right"] + data_epem_RE31["e_bin_left"])/2. 
        data_epem_RE31["E(dflux/dE)"] = data_epem_RE31["bin_center"]*data_epem_RE31["dfluxdE"]
        data_epem_RE31["dflux/dE"] = data_epem_RE31["dfluxdE"]
    


        

    stationname = ""

    if station != "28":

        if station == "22":
            stationname = "RE11"
        if station == "24":
            stationname = "RE12"
        if station == "26":
            stationname = "RE13"    
        if station == "30":
            stationname = "RE41"
        if station == "32":
            stationname = "RE42"            

        data_neut.to_csv(geometry_location+"datafiles/energy_"+stationname+"_EdfluxdE_neutrons.csv",columns = ["bin_center","E(dflux/dE)"])
        data_ph.to_csv(geometry_location+"datafiles/energy_"+stationname+"_EdfluxdE_photons.csv",columns = ["bin_center","E(dflux/dE)"])
        data_epem.to_csv(geometry_location+"datafiles/energy_"+stationname+"_EdfluxdE_ep.csv",columns = ["bin_center","E(dflux/dE)"])

        data_neut.to_csv(geometry_location+"datafiles/energy_"+stationname+"_dfluxdE_neutrons.csv",columns = ["bin_center","dflux/dE"])
        data_ph.to_csv(geometry_location+"datafiles/energy_"+stationname+"_dfluxdE_photons.csv",columns = ["bin_center","dflux/dE"])
        data_epem.to_csv(geometry_location+"datafiles/energy_"+stationname+"_dfluxdE_ep.csv",columns = ["bin_center","dflux/dE"])
            

        #plt.figure(1)  
        data_neut.plot(x="bin_center",y="E(dflux/dE)")
        plt.xscale('log')
        plt.yscale('log')
        plt.title(stationname)
        plt.savefig(geometry_location+"/plots/"+stationname+"_EdfluxdE_neut.png")
        data_ph.plot(x="bin_center",y="E(dflux/dE)")
        plt.xscale('log')
        plt.yscale('log')
        plt.title(stationname)
        plt.savefig(geometry_location+"/plots/"+stationname+"_EdfluxdE_ph.png")
        data_epem.plot(x="bin_center",y="E(dflux/dE)")
        plt.xscale('log')
        plt.yscale('log')
        plt.title(stationname)
        plt.savefig(geometry_location+"/plots/"+stationname+"_EdfluxdE_epem.png")


        data_neut.plot(x="bin_center",y="dflux/dE")
        plt.xscale('log')
        plt.yscale('log')
        plt.title(stationname)
        plt.savefig(geometry_location+"/plots/"+stationname+"_dfluxdE_neut.png")
        data_ph.plot(x="bin_center",y="dflux/dE")
        plt.xscale('log')
        plt.yscale('log')
        plt.title(stationname)
        plt.savefig(geometry_location+"/plots/"+stationname+"_dfluxdE_ph.png")
        data_epem.plot(x="bin_center",y="dflux/dE")
        plt.xscale('log')
        plt.yscale('log')
        plt.title(stationname)
        plt.savefig(geometry_location+"/plots/"+stationname+"_dfluxdE_epem.png")

    else: 
        data_neut_RE21.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE21")
        plt.savefig(geometry_location+"/plots/RE21_neut.png")
        data_ph_RE21.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE21")
        plt.savefig(geometry_location+"/plots/RE21_ph.png")
        data_epem_RE21.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE21")
        plt.savefig(geometry_location+"/plots/RE21_epem.png")
        
        data_neut_RE22.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE22")
        plt.savefig(geometry_location+"/plots/RE22_neut.png")
        data_ph_RE22.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE22")
        plt.savefig(geometry_location+"/plots/RE22_ph.png")
        data_epem_RE22.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE22")
        plt.savefig(geometry_location+"/plots/RE22_epem.png")
        
        data_neut_RE31.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE31")
        plt.savefig(geometry_location+"/plots/RE31_neut.png")
        data_ph_RE31.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE31")
        plt.savefig(geometry_location+"/plots/RE31_ph.png")
        data_epem_RE31.plot(x="bin_center",y="bin_center_xenergy")
        plt.xscale('log')
        plt.yscale('log')
        plt.title("RE31")
        plt.savefig(geometry_location+"/plots/RE31_epem.png")
        
        data_neut_RE21.to_csv(geometry_location+"datafiles/energy_RE21_neutrons.csv",columns = ["bin_center","bin_center_xenergy"])
        data_ph_RE21.to_csv(geometry_location+"datafiles/energy_RE21_photons.csv",columns = ["bin_center","bin_center_xenergy"])
        data_epem_RE21.to_csv(geometry_location+"datafiles/energy_RE21_ep.csv",columns = ["bin_center","bin_center_xenergy"])

        data_neut_RE22.to_csv(geometry_location+"datafiles/energy_RE22_neutrons.csv",columns = ["bin_center","bin_center_xenergy"])
        data_ph_RE22.to_csv(geometry_location+"datafiles/energy_RE22_photons.csv",columns = ["bin_center","bin_center_xenergy"])
        data_epem_RE22.to_csv(geometry_location+"datafiles/energy_RE22_ep.csv",columns = ["bin_center","bin_center_xenergy"])


        data_neut_RE31.to_csv(geometry_location+"datafiles/energy_RE31_neutrons.csv",columns = ["bin_center","bin_center_xenergy"])
        data_ph_RE31.to_csv(geometry_location+"datafiles/energy_RE31_photons.csv",columns = ["bin_center","bin_center_xenergy"])
        data_epem_RE31.to_csv(geometry_location+"datafiles/energy_RE31_ep.csv",columns = ["bin_center","bin_center_xenergy"])



    plt.show()

