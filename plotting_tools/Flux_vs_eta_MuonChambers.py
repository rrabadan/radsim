import numpy as np
import matplotlib as plt
import math
import pylab
import sys 

#geometry_location="C:/Users/Alfredo/source/repos/RadSim/geometries/v_3_0_0_0"
#geometry_location="C:/Users/Alfredo/source/repos/RadSim/geometries/v_3_31_4_0"
geometry_location="C:/Users/Alfredo/source/repos/RadSim/geometries/v_3_31_4_0/fulletarange"

#sensitivityneut= float(sys.argv[1])
#sensitivityph= float(sys.argv[2])
#sensitivityepem= float(sys.argv[3])

sensitivityneut = 0.0026
sensitivityph = 0.016
sensitivityepem = 0.35

#sensitivityneut = 1.0
#sensitivityph = 1.0
#sensitivityepem = 1.0

Lumi = 800000000.0*1.5

#####  limits on Z and R ranges 
zin_MB1 = np.loadtxt(geometry_location+"/MB1_neut.dat",usecols=(0))
zfin_MB1 = np.loadtxt(geometry_location+"/MB1_neut.dat",usecols=(1))

rin_RE1213 = np.loadtxt(geometry_location+"/RE1213_neut.dat",usecols=(0))
rin_RE2122 = np.loadtxt(geometry_location+"/RE2122_neut.dat",usecols=(0))
rin_RE3233 = np.loadtxt(geometry_location+"/RE3233_neut.dat",usecols=(0))
rin_RE4243 = np.loadtxt(geometry_location+"/RE4243_neut.dat",usecols=(0))

rfin_RE1213 = np.loadtxt(geometry_location+"/RE1213_neut.dat",usecols=(1))
rfin_RE2122 = np.loadtxt(geometry_location+"/RE2122_neut.dat",usecols=(1))
rfin_RE3233 = np.loadtxt(geometry_location+"/RE3233_neut.dat",usecols=(1))
rfin_RE4243 = np.loadtxt(geometry_location+"/RE4243_neut.dat",usecols=(1))


rin_ME1 = np.loadtxt(geometry_location+"/ME1213_neut.dat",usecols=(0))
rin_ME2 = np.loadtxt(geometry_location+"/ME2_neut.dat",usecols=(0))
rin_ME3 = np.loadtxt(geometry_location+"/ME3_neut.dat",usecols=(0))
rin_ME4 = np.loadtxt(geometry_location+"/ME4_neut.dat",usecols=(0))

rfin_ME1 = np.loadtxt(geometry_location+"/ME1213_neut.dat",usecols=(1))
rfin_ME2 = np.loadtxt(geometry_location+"/ME2_neut.dat",usecols=(1))
rfin_ME3 = np.loadtxt(geometry_location+"/ME3_neut.dat",usecols=(1))
rfin_ME4 = np.loadtxt(geometry_location+"/ME4_neut.dat",usecols=(1))



####### Flux DT chambers 
flux_MB1_neut = np.loadtxt(geometry_location+"/MB1_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_MB2_neut = np.loadtxt(geometry_location+"/MB2_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_MB3_neut = np.loadtxt(geometry_location+"/MB3_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_MB4_neut = np.loadtxt(geometry_location+"/MB4_neut.dat",usecols=(2))*Lumi*sensitivityneut

flux_MB1_ph = np.loadtxt(geometry_location+"/MB1_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_MB2_ph = np.loadtxt(geometry_location+"/MB2_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_MB3_ph = np.loadtxt(geometry_location+"/MB3_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_MB4_ph = np.loadtxt(geometry_location+"/MB4_ph.dat",usecols=(2))*Lumi*sensitivityph

flux_MB1_epem = np.loadtxt(geometry_location+"/MB1_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_MB2_epem = np.loadtxt(geometry_location+"/MB2_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_MB3_epem = np.loadtxt(geometry_location+"/MB3_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_MB4_epem = np.loadtxt(geometry_location+"/MB4_epem.dat",usecols=(2))*Lumi*sensitivityepem

#### Flux CSC chambers 

flux_ME1_neut = np.loadtxt(geometry_location+"/ME1213_neut.dat",usecols=(2))*Lumi
flux_ME2_neut = np.loadtxt(geometry_location+"/ME2_neut.dat",usecols=(2))*Lumi
flux_ME3_neut = np.loadtxt(geometry_location+"/ME3_neut.dat",usecols=(2))*Lumi
flux_ME4_neut = np.loadtxt(geometry_location+"/ME4_neut.dat",usecols=(2))*Lumi

flux_ME1_ph = np.loadtxt(geometry_location+"/ME1213_ph.dat",usecols=(2))*Lumi
flux_ME2_ph = np.loadtxt(geometry_location+"/ME2_ph.dat",usecols=(2))*Lumi
flux_ME3_ph = np.loadtxt(geometry_location+"/ME3_ph.dat",usecols=(2))*Lumi
flux_ME4_ph = np.loadtxt(geometry_location+"/ME4_ph.dat",usecols=(2))*Lumi

flux_ME1_epem = np.loadtxt(geometry_location+"/ME1213_epem.dat",usecols=(2))*Lumi
flux_ME2_epem = np.loadtxt(geometry_location+"/ME2_epem.dat",usecols=(2))*Lumi
flux_ME3_epem = np.loadtxt(geometry_location+"/ME3_epem.dat",usecols=(2))*Lumi
flux_ME4_epem = np.loadtxt(geometry_location+"/ME4_epem.dat",usecols=(2))*Lumi



flux_ME1_neut_neg = np.loadtxt(geometry_location+"/ME1213_neut_neg.dat",usecols=(2))*Lumi
flux_ME2_neut_neg = np.loadtxt(geometry_location+"/ME2_neut_neg.dat",usecols=(2))*Lumi
flux_ME3_neut_neg = np.loadtxt(geometry_location+"/ME3_neut_neg.dat",usecols=(2))*Lumi
flux_ME4_neut_neg = np.loadtxt(geometry_location+"/ME4_neut_neg.dat",usecols=(2))*Lumi

flux_ME1_ph_neg = np.loadtxt(geometry_location+"/ME1213_ph_neg.dat",usecols=(2))*Lumi
flux_ME2_ph_neg = np.loadtxt(geometry_location+"/ME2_ph_neg.dat",usecols=(2))*Lumi
flux_ME3_ph_neg = np.loadtxt(geometry_location+"/ME3_ph_neg.dat",usecols=(2))*Lumi
flux_ME4_ph_neg = np.loadtxt(geometry_location+"/ME4_ph_neg.dat",usecols=(2))*Lumi

flux_ME1_epem_neg = np.loadtxt(geometry_location+"/ME1213_epem_neg.dat",usecols=(2))*Lumi
flux_ME2_epem_neg = np.loadtxt(geometry_location+"/ME2_epem_neg.dat",usecols=(2))*Lumi
flux_ME3_epem_neg = np.loadtxt(geometry_location+"/ME3_epem_neg.dat",usecols=(2))*Lumi
flux_ME4_epem_neg = np.loadtxt(geometry_location+"/ME4_epem_neg.dat",usecols=(2))*Lumi

####### Flux RB chambers 
flux_RB1_neut = np.loadtxt(geometry_location+"/RB1top_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_RB2_neut = np.loadtxt(geometry_location+"/RB2top_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_RB3_neut = np.loadtxt(geometry_location+"/RB3_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_RB4_neut = np.loadtxt(geometry_location+"/RB4_neut.dat",usecols=(2))*Lumi*sensitivityneut

flux_RB1_ph = np.loadtxt(geometry_location+"/RB1top_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_RB2_ph = np.loadtxt(geometry_location+"/RB2top_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_RB3_ph = np.loadtxt(geometry_location+"/RB3_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_RB4_ph = np.loadtxt(geometry_location+"/RB4_ph.dat",usecols=(2))*Lumi*sensitivityph

flux_RB1_epem = np.loadtxt(geometry_location+"/RB1top_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_RB2_epem = np.loadtxt(geometry_location+"/RB2top_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_RB3_epem = np.loadtxt(geometry_location+"/RB3_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_RB4_epem = np.loadtxt(geometry_location+"/RB4_epem.dat",usecols=(2))*Lumi*sensitivityepem

######### Flux RE chambers
flux_RE1213_neut = np.loadtxt(geometry_location+"/RE1213_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_RE2122_neut = np.loadtxt(geometry_location+"/RE2122_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_RE3233_neut = np.loadtxt(geometry_location+"/RE3233_neut.dat",usecols=(2))*Lumi*sensitivityneut
flux_RE4243_neut = np.loadtxt(geometry_location+"/RE4243_neut.dat",usecols=(2))*Lumi*sensitivityneut

flux_RE1213_ph = np.loadtxt(geometry_location+"/RE1213_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_RE2122_ph = np.loadtxt(geometry_location+"/RE2122_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_RE3233_ph = np.loadtxt(geometry_location+"/RE3233_ph.dat",usecols=(2))*Lumi*sensitivityph
flux_RE4243_ph = np.loadtxt(geometry_location+"/RE4243_ph.dat",usecols=(2))*Lumi*sensitivityph

flux_RE1213_epem = np.loadtxt(geometry_location+"/RE1213_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_RE2122_epem = np.loadtxt(geometry_location+"/RE2122_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_RE3233_epem = np.loadtxt(geometry_location+"/RE3233_epem.dat",usecols=(2))*Lumi*sensitivityepem
flux_RE4243_epem = np.loadtxt(geometry_location+"/RE4243_epem.dat",usecols=(2))*Lumi*sensitivityepem


##### Flux RE negative side 
flux_RE1213_neut_neg = np.loadtxt(geometry_location+"/RE1213_neut_neg.dat",usecols=(2))*Lumi*sensitivityneut
flux_RE2122_neut_neg = np.loadtxt(geometry_location+"/RE2122_neut_neg.dat",usecols=(2))*Lumi*sensitivityneut
flux_RE3233_neut_neg = np.loadtxt(geometry_location+"/RE3233_neut_neg.dat",usecols=(2))*Lumi*sensitivityneut
flux_RE4243_neut_neg = np.loadtxt(geometry_location+"/RE4243_neut_neg.dat",usecols=(2))*Lumi*sensitivityneut

flux_RE1213_ph_neg = np.loadtxt(geometry_location+"/RE1213_ph_neg.dat",usecols=(2))*Lumi*sensitivityph
flux_RE2122_ph_neg = np.loadtxt(geometry_location+"/RE2122_ph_neg.dat",usecols=(2))*Lumi*sensitivityph
flux_RE3233_ph_neg = np.loadtxt(geometry_location+"/RE3233_ph_neg.dat",usecols=(2))*Lumi*sensitivityph
flux_RE4243_ph_neg = np.loadtxt(geometry_location+"/RE4243_ph_neg.dat",usecols=(2))*Lumi*sensitivityph

flux_RE1213_epem_neg = np.loadtxt(geometry_location+"/RE1213_epem_neg.dat",usecols=(2))*Lumi*sensitivityepem
flux_RE2122_epem_neg = np.loadtxt(geometry_location+"/RE2122_epem_neg.dat",usecols=(2))*Lumi*sensitivityepem
flux_RE3233_epem_neg = np.loadtxt(geometry_location+"/RE3233_epem_neg.dat",usecols=(2))*Lumi*sensitivityepem
flux_RE4243_epem_neg = np.loadtxt(geometry_location+"/RE4243_epem_neg.dat",usecols=(2))*Lumi*sensitivityepem

flux_MB1_total = flux_MB1_neut+flux_MB1_ph+flux_MB1_epem
flux_MB2_total = flux_MB2_neut+flux_MB2_ph+flux_MB2_epem
flux_MB3_total = flux_MB3_neut+flux_MB3_ph+flux_MB3_epem
flux_MB4_total = flux_MB4_neut+flux_MB4_ph+flux_MB4_epem

flux_ME1_total = flux_ME1_neut+flux_ME1_ph+flux_ME1_epem
flux_ME2_total = flux_ME2_neut+flux_ME2_ph+flux_ME2_epem
flux_ME3_total = flux_ME3_neut+flux_ME3_ph+flux_ME3_epem
flux_ME4_total = flux_ME4_neut+flux_ME4_ph+flux_ME4_epem

flux_ME1_total_neg = flux_ME1_neut_neg+flux_ME1_ph_neg+flux_ME1_epem_neg
flux_ME2_total_neg = flux_ME2_neut_neg+flux_ME2_ph_neg+flux_ME2_epem_neg
flux_ME3_total_neg = flux_ME3_neut_neg+flux_ME3_ph_neg+flux_ME3_epem_neg
flux_ME4_total_neg = flux_ME4_neut_neg+flux_ME4_ph_neg+flux_ME4_epem_neg

flux_RB1_total = flux_RB1_neut+flux_RB1_ph+flux_RB1_epem
flux_RB2_total = flux_RB2_neut+flux_RB2_ph+flux_RB2_epem
flux_RB3_total = flux_RB3_neut+flux_RB3_ph+flux_RB3_epem
flux_RB4_total = flux_RB4_neut+flux_RB4_ph+flux_RB4_epem

flux_RE1213_total = flux_RE1213_neut+flux_RE1213_ph+flux_RE1213_epem
flux_RE2122_total = flux_RE2122_neut+flux_RE2122_ph+flux_RE2122_epem
flux_RE3233_total = flux_RE3233_neut+flux_RE3233_ph+flux_RE3233_epem
flux_RE4243_total = flux_RE4243_neut+flux_RE4243_ph+flux_RE4243_epem

flux_RE1213_total_neg = flux_RE1213_neut_neg+flux_RE1213_ph_neg+flux_RE1213_epem_neg
flux_RE2122_total_neg = flux_RE2122_neut_neg+flux_RE2122_ph_neg+flux_RE2122_epem_neg
flux_RE3233_total_neg = flux_RE3233_neut_neg+flux_RE3233_ph_neg+flux_RE3233_epem_neg
flux_RE4243_total_neg = flux_RE4243_neut_neg+flux_RE4243_ph_neg+flux_RE4243_epem_neg

deltaR_MB1 = 425.
deltaR_MB2 = 520.
deltaR_MB3 = 620.
deltaR_MB4 = 720.

deltaZ_RE1213 = 685.
deltaZ_RE2122 = 790.
deltaZ_RE3233 = 975.
deltaZ_RE4243 = 1050.

deltaZ_ME1 = 700.
deltaZ_ME2 = 840.
deltaZ_ME3 = 930.
deltaZ_ME4 = 1020.

def etaconv_barrel(zin,zfin,deltaR):
    deltaz = (zin + zfin)/2.
    eta =-math.log( math.tan( (math.pi/4.) - ( math.atan( deltaz/deltaR)/2. )))
    return eta

def etaconv_endcap(rin,rfin,deltaZ):
    deltar = (rin+ rfin)/2.
    eta =-math.log( math.tan(  ( math.atan( deltar/deltaZ)/2.)))
    return eta

eta_MB1=[]
eta_MB2=[]
eta_MB3=[]
eta_MB4=[]

eta_ME1=[]
eta_ME2=[]
eta_ME3=[]
eta_ME4=[]

eta_ME1_neg=[]
eta_ME2_neg=[]
eta_ME3_neg=[]
eta_ME4_neg=[]

eta_RE1213=[]
eta_RE2122=[]
eta_RE3233=[]
eta_RE4243=[]

eta_RE1213_neg=[]
eta_RE2122_neg=[]
eta_RE3233_neg=[]
eta_RE4243_neg=[]


for i in range(0,133):
    etatemp_MB1 = etaconv_barrel(zin_MB1[i],zfin_MB1[i],deltaR_MB1)
    etatemp_MB2 = etaconv_barrel(zin_MB1[i],zfin_MB1[i],deltaR_MB2)
    etatemp_MB3 = etaconv_barrel(zin_MB1[i],zfin_MB1[i],deltaR_MB3)
    etatemp_MB4 = etaconv_barrel(zin_MB1[i],zfin_MB1[i],deltaR_MB4)
    eta_MB1.append(etatemp_MB1)
    eta_MB2.append(etatemp_MB2)
    eta_MB3.append(etatemp_MB3)
    eta_MB4.append(etatemp_MB4)

for i in range(0,44):
    etatemp_RE1213 = etaconv_endcap(rin_RE1213[i],rfin_RE1213[i],deltaZ_RE1213)
    etatemp_ME1 = etaconv_endcap(rin_ME1[i],rfin_ME1[i],deltaZ_ME1)
    eta_RE1213.append(etatemp_RE1213)
    eta_RE1213_neg.append(-etatemp_RE1213)
    eta_ME1.append(etatemp_ME1)
    eta_ME1_neg.append(-etatemp_ME1)


for i in range(0,37):
    etatemp_RE2122 = etaconv_endcap(rin_RE2122[i],rfin_RE2122[i],deltaZ_RE2122)
    etatemp_RE3233 = etaconv_endcap(rin_RE3233[i],rfin_RE3233[i],deltaZ_RE3233)
    etatemp_RE4243 = etaconv_endcap(rin_RE4243[i],rfin_RE4243[i],deltaZ_RE4243)
    
    eta_RE2122.append(etatemp_RE2122)
    eta_RE3233.append(etatemp_RE3233)
    eta_RE4243.append(etatemp_RE4243)

    eta_RE2122_neg.append(-etatemp_RE2122)
    eta_RE3233_neg.append(-etatemp_RE3233)
    eta_RE4243_neg.append(-etatemp_RE4243)


for i in range(0,56):
    etatemp_ME2 = etaconv_endcap(rin_ME2[i],rfin_ME2[i],deltaZ_ME2)
    eta_ME2.append(etatemp_ME2)
    eta_ME2_neg.append(-etatemp_ME2)
for i in range(0,55):
    etatemp_ME3 = etaconv_endcap(rin_ME3[i],rfin_ME3[i],deltaZ_ME3)
    eta_ME3.append(etatemp_ME3)
    eta_ME3_neg.append(-etatemp_ME3)
for i in range(0,53):
    etatemp_ME4 = etaconv_endcap(rin_ME3[i],rfin_ME4[i],deltaZ_ME4)
    eta_ME4.append(etatemp_ME4)
    eta_ME4_neg.append(-etatemp_ME4)


#CMSGeomTag = "CMS-FLUKA study v.3.0.0.0"  
CMSGeomTag = "CMS-FLUKA study v.3.31.4.0" 
    
plt.pyplot.figure(1)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (10**2,10**5) )
else:
    plt.pyplot.ylim( (10**0,10**3) )

plt.pyplot.plot(eta_MB1,flux_RB1_total,label='RB1',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE1213,flux_RE1213_total,label='RE1213 (z>0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE1213_neg,flux_RE1213_total_neg,label='RE1213 (z<0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)

plt.pyplot.xlabel(r'$|\eta$|',fontsize=18,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RB1_vs_RE1_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RB1_vs_RE1_total_rate.png")


plt.pyplot.figure(2)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (50,10**5) )
else:
    plt.pyplot.ylim( (0.5,10**3) )


plt.pyplot.plot(eta_MB2,flux_RB2_total,label='RB2',color="brown",marker="s",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE2122,flux_RE2122_total,label='RE2122 (z>0)',color="brown",marker="s",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE2122_neg,flux_RE2122_total_neg,label='RE2122 (z<0)',color="brown",marker="s",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)

plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RB2_vs_RE2_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RB2_vs_RE2_total_rate.png")


plt.pyplot.figure(3)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (20,10**5) )
else:
    plt.pyplot.ylim( (10**-1,10**3) )

plt.pyplot.plot(eta_MB3,flux_RB3_total,label='RB3',color="magenta",marker="^",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE3233,flux_RE3233_total,label='RE3233 (z>0)',color="magenta",marker="^",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE3233_neg,flux_RE3233_total_neg,label='RE3233 (z<0)',color="magenta",marker="^",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)


plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RB3_vs_RE3_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RB3_vs_RE3_total_rate.png")


plt.pyplot.figure(4)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (10**3,10**5) )
else:
    plt.pyplot.ylim( (2,10**3) )

plt.pyplot.plot(eta_MB4,flux_RB4_total,label='RB3',color="gray",marker="v",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE4243,flux_RE4243_total,label='RE3233 (z>0)',color="gray",marker="v",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE4243_neg,flux_RE4243_total_neg,label='RE3233 (z<0)',color="gray",marker="v",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)


plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RB4_vs_RE4_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RB4_vs_RE4_total_rate.png")


plt.pyplot.figure(5)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (10**2,10**4) )
else:
    plt.pyplot.ylim( (2,10**3) )

plt.pyplot.plot(eta_ME1,flux_ME1_total,label='ME1 z>0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_ME1_neg,flux_ME1_total_neg,label='ME1 z<0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE1213,flux_RE1213_total,label='RE1213 (z>0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE1213_neg,flux_RE1213_total_neg,label='RE1213 (z<0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)

plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RE1_vs_ME1_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RE1_vs_ME1_total_rate.png")



plt.pyplot.figure(6)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (10**2,10**5) )
else:
    plt.pyplot.ylim( (2,10**3) )

plt.pyplot.plot(eta_ME2,flux_ME2_total,label='ME2 z>0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_ME2_neg,flux_ME2_total_neg,label='ME2 z<0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE2122,flux_RE2122_total,label='RE2122 (z>0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE2122_neg,flux_RE2122_total_neg,label='RE2122 (z<0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)


plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RE2_vs_ME2_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RE2_vs_ME2_total_rate.png")                    


plt.pyplot.figure(7)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (10**3,10**5) )
else:
    plt.pyplot.ylim( (2,10**3) )

plt.pyplot.plot(eta_ME3,flux_ME3_total,label='ME3 z>0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_ME3_neg,flux_ME3_total_neg,label='ME3 z<0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE3233,flux_RE3233_total,label='RE3233 (z>0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE3233_neg,flux_RE3233_total_neg,label='RE3233 (z<0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)


plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RE3_vs_ME3_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RE3_vs_ME3_total_rate.png")


plt.pyplot.figure(8)    
if sensitivityneut == 1.0:
    plt.pyplot.ylim( (10**3,10**5) )
else:
    plt.pyplot.ylim( (2,10**3) )

plt.pyplot.plot(eta_ME4,flux_ME4_total,label='ME4 z>0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_ME4_neg,flux_ME4_total_neg,label='ME4 z<0',color="red",marker="o",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE4243,flux_RE4243_total,label='RE4243 (z>0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')
plt.pyplot.plot(eta_RE4243_neg,flux_RE4243_total_neg,label='RE4243 (z<0)',color="red",marker="o",fillstyle="none",linestyle="none")
plt.pyplot.yscale('log')

plt.pyplot.xticks(fontsize=16)
plt.pyplot.yticks(fontsize=16)
plt.pyplot.legend(fontsize=16)

plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
if sensitivityneut == 1.0: 
    plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=18)
else:
    plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=18)
    
plt.pyplot.tight_layout()
plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
plt.pyplot.text(0.2,0.9,"L=1.5x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
if sensitivityneut == 1.0:
    plt.pyplot.savefig(geometry_location+"/plots/RE3_vs_ME3_total_flux.png")
else:
    plt.pyplot.savefig(geometry_location+"/plots/RE3_vs_ME3_total_rate.png")                        
    

# plt.pyplot.figure(1)    
# if sensitivityneut == 1.0:
#     plt.pyplot.ylim(0,5000)
# else:
#    plt.pyplot.ylim(0,5)
# plt.pyplot.plot(eta_MB1,flux_MB1_neut,label='RB1 R[400-460cm]',color="blue")
# plt.pyplot.plot(eta_MB2,flux_MB2_neut,label='RB2 R[490-540cm]',color="red")
# plt.pyplot.plot(eta_MB2,flux_MB3_neut,label='RB3 R[600-610cm]',color="orange")
# plt.pyplot.plot(eta_MB2,flux_MB4_neut,label='RB4 R[700-710cm]',color="black")

# plt.pyplot.plot(eta_RE1213,flux_RE1213_neut,label='RE1213 Z[680-690cm]',color="blue")
# plt.pyplot.plot(eta_RE2122,flux_RE2122_neut,label='RE2122 Z[780-800cm]',color="red")
# plt.pyplot.plot(eta_RE3233,flux_RE3233_neut,label='RE3233 Z[970-980cm]',color="orange")
# plt.pyplot.plot(eta_RE4243,flux_RE4243_neut,label='RE4243 Z[1060-1070cm]',color="black")

# plt.pyplot.plot(eta_RE1213_neg,flux_RE1213_neut_neg,label='RE1213 Z[680-690cm]',color='blue')
# plt.pyplot.plot(eta_RE2122_neg,flux_RE2122_neut_neg,label='RE2122 Z[780-800cm]',color="red")
# plt.pyplot.plot(eta_RE3233_neg,flux_RE3233_neut_neg,label='RE3233 Z[970-980cm]',color="orange")
# plt.pyplot.plot(eta_RE4243_neg,flux_RE4243_neut_neg,label='RE4243 Z[1060-1070cm]',color="black")

# plt.pyplot.legend()
# plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
# if sensitivityneut == 1.0: 
#     plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=14)
# else:
#     plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=14)
    
# plt.pyplot.tight_layout()
# plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# plt.pyplot.text(0.6,0.8,"L=1x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# if sensitivityneut == 1.0:
#     plt.pyplot.savefig(geometry_location+"/plots/neut_flux.png")
# else:
#     plt.pyplot.savefig(geometry_location+"/plots/neut_rate.png")
    

# plt.pyplot.figure(2)
# if sensitivityneut == 1.0:
#     plt.pyplot.ylim(0,2500)
# else:
#     plt.pyplot.ylim(0,25)
# plt.pyplot.plot(eta_MB1,flux_MB1_ph,label='RB1 R[400-460cm]',marker=".")
# plt.pyplot.plot(eta_MB2,flux_MB2_ph,label='RB2 R[490-540cm]')
# plt.pyplot.plot(eta_MB2,flux_MB3_ph,label='RB3 R[600-610cm]')
# plt.pyplot.plot(eta_MB2,flux_MB4_ph,label='RB4 R[700-710cm]')

# plt.pyplot.plot(eta_RE1213,flux_RE1213_ph,label='RE1213 Z[680-690cm]',color='b')
# plt.pyplot.plot(eta_RE2122,flux_RE2122_ph,label='RE2122 Z[780-800cm]')
# plt.pyplot.plot(eta_RE3233,flux_RE3233_ph,label='RE3233 Z[970-980cm]')
# plt.pyplot.plot(eta_RE4243,flux_RE4243_ph,label='RE4243 Z[1060-1070cm]')

# plt.pyplot.plot(eta_RE1213_neg,flux_RE1213_ph_neg,label='RE1213 Z[680-690cm]',color='b')
# plt.pyplot.plot(eta_RE2122_neg,flux_RE2122_ph_neg,label='RE2122 Z[780-800cm]')
# plt.pyplot.plot(eta_RE3233_neg,flux_RE3233_ph_neg,label='RE3233 Z[970-980cm]')
# plt.pyplot.plot(eta_RE4243_neg,flux_RE4243_ph_neg,label='RE4243 Z[1060-1070cm]')


# plt.pyplot.legend()
# plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
# if sensitivityneut == 1.0:
#     plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=14)
# else:
#     plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=14)
# plt.pyplot.tight_layout()
# plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# plt.pyplot.text(0.6,0.8,"L=1x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# if sensitivityneut == 1.0:
#     plt.pyplot.savefig(geometry_location+"/plots/ph_flux.png")
# else:
#     plt.pyplot.savefig(geometry_location+"/plots/ph_rate.png")


# plt.pyplot.figure(3)
# if sensitivityneut == 1.0:
#     plt.pyplot.ylim(0,50)
# else:
#     plt.pyplot.ylim(0,25)
# plt.pyplot.plot(eta_MB1,flux_MB1_epem,label='RB1 R[400-460cm]',marker=".")
# plt.pyplot.plot(eta_MB2,flux_MB2_epem,label='RB2 R[490-540cm]')
# plt.pyplot.plot(eta_MB2,flux_MB3_epem,label='RB3 R[600-610cm]')
# plt.pyplot.plot(eta_MB2,flux_MB4_epem,label='RB4 R[700-710cm]')

# plt.pyplot.plot(eta_RE1213,flux_RE1213_epem,label='RE1213 Z[680-690cm]',color='b')
# plt.pyplot.plot(eta_RE2122,flux_RE2122_epem,label='RE2122 Z[780-800cm]')
# plt.pyplot.plot(eta_RE3233,flux_RE3233_epem,label='RE3233 Z[970-980cm]')
# plt.pyplot.plot(eta_RE4243,flux_RE4243_epem,label='RE4243 Z[1060-1070cm]')

# plt.pyplot.plot(eta_RE1213_neg,flux_RE1213_epem_neg,label='RE1213 Z[680-690cm]',color='b')
# plt.pyplot.plot(eta_RE2122_neg,flux_RE2122_epem_neg,label='RE2122 Z[780-800cm]')
# plt.pyplot.plot(eta_RE3233_neg,flux_RE3233_epem_neg,label='RE3233 Z[970-980cm]')
# plt.pyplot.plot(eta_RE4243_neg,flux_RE4243_epem_neg,label='RE4243 Z[1060-1070cm]')

# plt.pyplot.legend()
# plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
# if sensitivityneut == 1.0:
#     plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=14)
# else:
#     plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=14)
# plt.pyplot.tight_layout()
# plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# plt.pyplot.text(0.6,0.8,"L=1x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# if sensitivityneut == 1.0:
#     plt.pyplot.savefig(geometry_location+"/plots/epem_flux.png")
# else:
#     plt.pyplot.savefig(geometry_location+"/plots/epem_rate.png")



# plt.pyplot.figure(4)
# if sensitivityneut == 1.0:
#     plt.pyplot.ylim(0,5000)
# else:
#     plt.pyplot.ylim(0,70)
# plt.pyplot.plot(eta_MB1,flux_MB1_total,label='RB1 R[400-460cm]',markers='.")
# plt.pyplot.plot(eta_MB2,flux_MB2_total,label='RB2 R[490-540cm]')
# plt.pyplot.plot(eta_MB2,flux_MB3_total,label='RB3 R[600-610cm]')
# plt.pyplot.plot(eta_MB2,flux_MB4_total,label='RB4 R[700-710cm]')

# plt.pyplot.plot(eta_RE1213,flux_RE1213_total,label='RE1213 Z[680-690cm]',color='b')
# plt.pyplot.plot(eta_RE2122,flux_RE2122_total,label='RE2122 Z[780-800cm]')
# plt.pyplot.plot(eta_RE3233,flux_RE3233_total,label='RE3233 Z[970-980cm]')
# plt.pyplot.plot(eta_RE4243,flux_RE4243_total,label='RE4243 Z[1060-1070cm]')

# plt.pyplot.plot(eta_RE1213_neg,flux_RE1213_total_neg,label='RE1213 Z[680-690cm]',color='b')
# plt.pyplot.plot(eta_RE2122_neg,flux_RE2122_total_neg,label='RE2122 Z[780-800cm]')
# plt.pyplot.plot(eta_RE3233_neg,flux_RE3233_total_neg,label='RE3233 Z[970-980cm]')
# plt.pyplot.plot(eta_RE4243_neg,flux_RE4243_total_neg,label='RE4243 Z[1060-1070cm]')

# plt.pyplot.legend()
# plt.pyplot.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
# if sensitivityneut == 1.0:
#     plt.pyplot.ylabel("Flux [Hz/$cm^{2}$]",fontsize=14)
# else:
#     plt.pyplot.ylabel("Rate [Hz/$cm^{2}$]",fontsize=14)
# plt.pyplot.tight_layout()
# plt.pyplot.text(0.15,0.03,CMSGeomTag,fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# plt.pyplot.text(0.6,0.8,"L=1x10E34cm$^{-2}$s$^{-1}$",fontsize=11,family="sans-serif",style="normal",weight='normal',transform=plt.pyplot.gcf().transFigure)
# if sensitivityneut == 1.0:
#     plt.pyplot.savefig(geometry_location+"/plots/total_flux.png")
# else:
#     plt.pyplot.savefig(geometry_location+"/plots/total_rate.png")    



plt.pyplot.show()

